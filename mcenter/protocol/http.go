package protocol

import (
	"context"
	"gitee.com/wshadm/devcloud-mini/mcenter/conf"
	"gitee.com/wshadm/devcloud-mini/mcenter/logger"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

func NewHttp(r *gin.Engine) *Http {
	return &Http{
		server: &http.Server{
			Handler:           r,
			Addr:              conf.C().Http.Address(),
			ReadHeaderTimeout: 30 * time.Second,
			WriteTimeout:      30 * time.Second,
		},
	}
}

//定义httpserver的属性

type Http struct {
	server *http.Server
}

func (h *Http) Start() error {
	logger.L().Debug().Msgf("http sever: %s", conf.C().Http.Address())
	return h.server.ListenAndServe()
}
func (h *Http) Stop(ctx context.Context) {
	// 服务的优雅关闭, 先关闭监听,新的请求就进不来, 等待老的请求 处理完成
	// 自己介绍来自操作系统的信号量 来决定 你的服务是否需要关闭
	// nginx  reload, os reload sign,  config.reload()
	// os termnial sign, 过了terminal的超时时间会直接kill
	h.server.Shutdown(ctx)
}
