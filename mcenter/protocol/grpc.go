package protocol

import (
	"context"
	"gitee.com/wshadm/devcloud-mini/mcenter/conf"
	"gitee.com/wshadm/devcloud-mini/mcenter/logger"
	"github.com/infraboard/mcube/ioc"
	"google.golang.org/grpc"
	"net"
)

func NewGrpc() *Grpc {
	return &Grpc{
		server: grpc.NewServer(),
	}
}

type Grpc struct {
	server *grpc.Server
}

func (g *Grpc) Start() error {
	ioc.LoadGrpcController(g.server)
	logger.L().Debug().Msgf("loaded controllers: %s", ioc.ListControllerObjectNames())
	// 2.3 启动grpc server
	lis, err := net.Listen("tcp", conf.C().Grpc.Address())
	if err != nil {
		return err
	}
	logger.L().Debug().Msgf("grpc server listen: %s", conf.C().Grpc.Address())
	return g.server.Serve(lis)
}

func (g *Grpc) Stop(ctx context.Context) {
	g.server.GracefulStop()
}
