package start

import (
	"context"
	"gitee.com/wshadm/devcloud-mini/mcenter/conf"
	"gitee.com/wshadm/devcloud-mini/mcenter/logger"
	"gitee.com/wshadm/devcloud-mini/mcenter/protocol"
	"github.com/gin-gonic/gin"
	"github.com/infraboard/mcube/ioc"
	"github.com/spf13/cobra"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	// 把所有的业务模块全部加载
	_ "gitee.com/wshadm/devcloud-mini/mcenter/app"
)

var (
	configType string
	configFile string
)

var Cmd = &cobra.Command{
	Use:   "start",
	Short: "mcenter  start [command]",
	Long:  "",
	Run: func(cmd *cobra.Command, args []string) {
		// 1. 加载全局配置
		switch configType {
		case "env":
			_, err := conf.LoadConfigFromEnv()
			cobra.CheckErr(err)
		default:
			// 默认通过配置文件加载
			_, err := conf.LoadConfigFromToml(configFile)
			cobra.CheckErr(err)
		}
		// 加载业务逻辑实现
		r := gin.Default()
		//启动http
		httpServer := protocol.NewHttp(r)
		go func() {
			httperr := httpServer.Start()
			if httperr != http.ErrServerClosed {
				cobra.CheckErr(httperr)
			}
		}()
		//  启动GRPC server
		grpcServer := protocol.NewGrpc()
		go func() {
			grpcErr := grpcServer.Start()
			cobra.CheckErr(grpcErr)
		}()
		err := ioc.InitIocObject()
		cobra.CheckErr(err)
		// 信号处理 负责阻塞住主进行(Loop Os Signal)
		// 处理信号量
		ch := make(chan os.Signal, 1)
		// 通知通道 ch，当接收到 SIGTERM, SIGINT, SIGHUP, SIGQUIT 时，将信号发送到通道
		signal.Notify(ch, syscall.SIGTERM, syscall.SIGINT, syscall.SIGHUP, syscall.SIGQUIT, syscall.SIGKILL)
		// 从chanel中获取信号
		s := <-ch
		logger.L().Info().Msgf("receive os signal: %s, exit ...", s)
		// 做后续处理
		ctx := context.Background()
		httpServer.Stop(ctx)
		grpcServer.Stop(ctx)
		// 其他资源释放逻辑
		conf.C().MySQL.Close()
	},
}

func init() {
	Cmd.Flags().StringVarP(&configType, "config-type", "t", "file", "程序加载配置的方式")
	Cmd.Flags().StringVarP(&configFile, "config-file", "f", "etc/config.toml", "配置文件的路径")
}
