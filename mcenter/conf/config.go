package conf

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	orm_mysql "gorm.io/driver/mysql"
	"gorm.io/gorm"
	"sync"
	"time"
)

func DefaultConfig() *Config {
	return &Config{
		MySQL: newDefaultMysql(),
		Http:  newDefaultHttp(),
		Grpc:  NewDefaultGrpc(),
	}
}

type Config struct {
	//[mysql]
	MySQL *MySQL `json:"mysql" toml:"mysql"`
	//[http]
	Http *Http `json:"http" toml:"http"`
	//[grpc]
	Grpc *Grpc `json:"grpc" toml:"grpc"`
}

// 格式化成json
func (c *Config) String() string {
	indent, _ := json.MarshalIndent(c, "", "\t")
	return string(indent)
}

type MySQL struct {
	Host     string `json:"host" toml:"host" env:"MYSQL_HOST"`
	Port     string `json:"port" toml:"port" env:"MYSQL_PORT"`
	DB       string `json:"db" toml:"db" env:"MYSQL_DB"`
	Username string `json:"username" toml:"username" env:"MYSQL_USERNAME"`
	Password string `json:"password" toml:"password" env:"MYSQL_PASSWORD"`
	//高级参数
	MaxOpenConn int `toml:"max_open_conn" env:"MYSQL_MAX_OPEN_CONN"`
	MaxIdleConn int `toml:"max_idle_conn" env:"MYSQL_MAX_IDLE_CONN"`
	MaxLifeTime int `toml:"max_life_time" env:"MYSQL_MAX_LIFE_TIME"`
	MaxIdleTime int `toml:"max_idle_time" env:"MYSQL_MAX_IDLE_TIME"`
	//并发安全
	lock sync.RWMutex
	db   *gorm.DB
}

func (m *MySQL) GetConnPool() (*sql.DB, error) {
	//var err error
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&multiStatements=true",
		m.Username, m.Password, m.Host, m.Port, m.DB)
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		return nil, fmt.Errorf("conection to mysql<%s> is err", dsn, err.Error())
	}
	//对连接池进行设置
	db.SetMaxOpenConns(m.MaxOpenConn)
	db.SetMaxIdleConns(m.MaxIdleConn)
	if m.MaxLifeTime != 0 {
		db.SetConnMaxIdleTime(time.Second * time.Duration(m.MaxLifeTime))
	}
	if m.MaxIdleConn != 0 {
		db.SetConnMaxIdleTime(time.Second * time.Duration(m.MaxIdleTime))
	}

	// 加了一个Ping
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := db.PingContext(ctx); err != nil {
		return nil, fmt.Errorf("ping mysql<%s> error, %s",
			dsn, err.Error())
	}
	return db, nil
}

func (m *MySQL) ORM() *gorm.DB {
	m.lock.Lock()
	defer m.lock.Unlock()

	if m.db == nil {
		// 初始化DB
		// 1.1 获取sql.DB
		p, err := m.GetConnPool()
		if err != nil {
			panic(err)
		}

		// 1.2 使用pool 初始化orm db对象
		m.db, err = gorm.Open(orm_mysql.New(orm_mysql.Config{
			Conn: p,
		}), &gorm.Config{
			// 执行任何 SQL 时都创建并缓存预编译语句，可以提高后续的调用速度
			PrepareStmt: true,
			// 对于写操作（创建、更新、删除），为了确保数据的完整性，GORM 会将它们封装在事务内运行。
			// 但这会降低性能，如果没有这方面的要求，您可以在初始化时禁用它，这将获得大约 30%+ 性能提升
			SkipDefaultTransaction: true,
			// 要有效地插入大量记录，请将一个 slice 传递给 Create 方法
			// CreateBatchSize: 200,
		})
		if err != nil {
			panic(err)
		}
	}
	return m.db
}

func (m *MySQL) Close() error {
	m.lock.Lock()
	defer m.lock.Unlock()
	if m.db != nil {
		//关闭底层连接池
		sqlDB, err := m.db.DB()
		if err != nil {
			return fmt.Errorf("failed to retrieve underlying sql.DB: %v", err)
		}
		// 调用 sql.DB 的 Close 方法关闭数据库连接池
		if err := sqlDB.Close(); err != nil {
			return fmt.Errorf("failed to close database connection: %v", err)
		}
	}
	return nil
}

// mysql的默认配置
func newDefaultMysql() *MySQL {
	return &MySQL{
		Host:     "127.0.0.1",
		Port:     "3306",
		DB:       "mcenter",
		Username: "root",
		Password: "123456",
	}
}

// http的默认配置
func newDefaultHttp() *Http {
	return &Http{
		Host: "127.0.0.1",
		Port: 8010,
	}
}

type Http struct {
	Host string `json:"host" toml:"host" env:"HTTP_HOST"`
	Port int    `json:"port" toml:"port" env:"HTTP_PORT"`
}

// 把地址和端口拼接成一个地址
func (h *Http) Address() string {
	return fmt.Sprintf("%s:%d", h.Host, h.Port)
}

func NewDefaultGrpc() *Grpc {
	return &Grpc{
		Host: "127.0.0.1",
		Port: 18010,
	}
}

type Grpc struct {
	Host string `json:"host" toml:"host" env:"GRPC_HOST"`
	Port int    `json:"port" toml:"port" env:"GRPC_PORT"`
}

func (g *Grpc) Address() string {
	return fmt.Sprintf("%s:%d", g.Host, g.Port)
}
