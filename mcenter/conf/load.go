package conf

import (
	"github.com/BurntSushi/toml"
	"github.com/caarlos0/env/v6"
)

var config *Config

//之前使用返回中带error时候，会导致在其他包中通过conf.C()找不到当中的配置对象，例如找不到conf.C().Http
//在带有 error 返回值时，IDE 在代码补全和类型检查时会考虑返回的 *Config 有可能为 nil，
//而你在没有处理 error 的情况下直接访问了 conf.C().Http。这使得 IDE 不确定 Http 是否可用，所以它不提供补全。
//当你去掉 error，IDE 确定返回的是一个有效的 *Config，所以能正常地找到 Http 字段。
//func C() (*Config, error) {
//	if config == nil {
//		return nil, fmt.Errorf("请加载程序配置, LoadConfigFromToml/LoadConfigFromEnv")
//	}
//	return config, nil
//}

func C() *Config {
	if config == nil {
		panic("请加载程序配置, LoadConfigFromToml/LoadConfigFromEnv")
	}
	return config
}

// 从Toml文件中获取配置
func LoadConfigFromToml(path string) (*Config, error) {
	conf := DefaultConfig()
	_, err := toml.DecodeFile(path, conf)
	if err != nil {
		return nil, err
	}
	config = conf //赋值给全局config
	return config, nil

}

func LoadConfigFromEnv() (*Config, error) {
	conf := DefaultConfig()
	err := env.Parse(conf)
	if err != nil {
		return nil, err
	}
	config = conf
	return config, nil
}
